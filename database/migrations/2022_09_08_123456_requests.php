<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

       
        Schema::create('translation_requests', function (Blueprint $table) {
            $table->id();
            $table->string('language_id');
            $table->string('key_id');
            $table->boolean('screenshot')->default(0);
            $table->boolean('fullfilled')->default(0);
            $table->foreignId('requester_id')->nullable();
            $table->foreignId('author_id')->nullable();
            $table->timestamps();
        });
     
       
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('translation_requests');
        Schema::enableForeignKeyConstraints();
    }
};