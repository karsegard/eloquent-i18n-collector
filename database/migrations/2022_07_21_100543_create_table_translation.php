<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('translation_keys', function (Blueprint $table) {
            $table->id();
            $table->string('name')->charset('utf8mb4')->collation('utf8mb4_bin');
            $table->string('comment')->nullable();
            $table->foreignId('category_id')->constrained('translation_categories')->onDelete('cascade');
            $table->timestamps();
        });
        
        Schema::create('translation_languages', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('translation_categories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('app_id')->constrained('translation_apps')->onDelete('cascade');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('translation_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('key_id')->constrained('translation_keys')->onDelete('cascade');
            $table->foreignId('language_id')->constrained('translation_languages')->onDelete('cascade');
            $table->string('value')->nullable()->charset('utf8mb4')->collation('utf8mb4_bin');
      //      $table->string('version');
            $table->timestamps();
        });

        Schema::create('translation_apps', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('translation_category_languages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id')->constrained('translation_categories')->onDelete('cascade');
            $table->foreignId('language_id')->constrained('translation_languages')->onDelete('cascade');
        });

        Schema::create('translation_contributor_categories', function (Blueprint $table) {
            $table->numericMorphs('contributor','contribcat');
            $table->foreignId('category_id')->constrained('translation_categories')->onDelete('cascade');
        });

        Schema::create('translation_contributor_languages', function (Blueprint $table) {
            $table->numericMorphs('contributor','contriblang');
            $table->foreignId('language_id')->constrained('translation_languages')->onDelete('cascade');
        });
       
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('translation_keys');
        Schema::dropIfExists('translation_languages');
        Schema::dropIfExists('translation_categories');
        Schema::dropIfExists('translation_translations');
        Schema::dropIfExists('translation_apps');
        Schema::dropIfExists('translation_category_languages');
        Schema::dropIfExists('translation_contributor_categories');
        Schema::dropIfExists('translation_contributor_languages');
     
        Schema::enableForeignKeyConstraints();
    }
};