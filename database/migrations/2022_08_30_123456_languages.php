<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

       
        Schema::table('translation_languages', function (Blueprint $table) {
            $table->renameColumn('name','key');
        });
        Schema::table('translation_languages', function (Blueprint $table) {
            $table->string('name')->nullable();
        });
       
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('translation_languages', function (Blueprint $table) {
            $table->id();
            $table->dropColumn('name');

            $table->renameColumn('key','name');
        });
        Schema::enableForeignKeyConstraints();
    }
};