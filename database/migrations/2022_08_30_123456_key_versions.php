<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

       
        Schema::create('translation_key_versions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('key_id');
            $table->string('version');
        });
       
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('translation_key_versions');
        Schema::enableForeignKeyConstraints();
    }
};