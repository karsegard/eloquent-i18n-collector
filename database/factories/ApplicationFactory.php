<?php

namespace KDA\Eloquent\I18nCollector\Database\Factories;

use KDA\Eloquent\I18nCollector\Models\Application;
use Illuminate\Database\Eloquent\Factories\Factory;
class ApplicationFactory extends Factory
{
    protected $model = Application::class;

    public function definition()
    {
        return [
            //
            'name'=>$this->faker->word(2)
        ];
    }
}
