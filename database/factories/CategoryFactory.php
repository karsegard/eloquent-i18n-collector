<?php

namespace KDA\Eloquent\I18nCollector\Database\Factories;

use KDA\Eloquent\I18nCollector\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Eloquent\I18nCollector\Models\Application;

class CategoryFactory extends Factory
{
    protected $model = Category::class;

    public function definition()
    {
        return [
            'name'=>$this->faker->word(),
            'app_id'=> Application::factory()
        ];
    }
}
