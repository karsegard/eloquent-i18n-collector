<?php

namespace KDA\Eloquent\I18nCollector\Database\Factories;

use KDA\Eloquent\I18nCollector\Models\Language;
use Illuminate\Database\Eloquent\Factories\Factory;
class LanguageFactory extends Factory
{
    protected $model = Language::class;

    public function definition()
    {
        return [
            //
            'name'=>$this->faker->languageCode()
        ];
    }
}
