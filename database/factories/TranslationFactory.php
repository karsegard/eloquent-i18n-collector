<?php

namespace KDA\Eloquent\I18nCollector\Database\Factories;

use KDA\Eloquent\I18nCollector\Models\Translation;
use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Eloquent\I18nCollector\Models\Key;
use KDA\Eloquent\I18nCollector\Models\Language;

class TranslationFactory extends Factory
{
    protected $model = Translation::class;

    public function definition()
    {
        return [
            //
        //    'category_id'=>Category::factory(),
            'key_id'=>Key::factory(),
            'language_id'=> Language::factory(),
            'value'=>$this->faker->title()
        ];
    }
}
