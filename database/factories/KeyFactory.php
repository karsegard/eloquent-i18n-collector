<?php

namespace KDA\Eloquent\I18nCollector\Database\Factories;

use KDA\Eloquent\I18nCollector\Models\Key;
use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Eloquent\I18nCollector\Models\Category;

class KeyFactory extends Factory
{
    protected $model = Key::class;

    public function definition()
    {
        return [
            'category_id'=>Category::factory(),
            'name'=>$this->faker->word(6)
            //
        ];
    }
}
