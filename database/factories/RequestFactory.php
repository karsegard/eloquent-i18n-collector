<?php

namespace KDA\Eloquent\I18nCollector\Database\Factories;

use KDA\Eloquent\I18nCollector\Models\Request;
use Illuminate\Database\Eloquent\Factories\Factory;
class RequestFactory extends Factory
{
    protected $model = Request::class;

    public function definition()
    {
        return [
          
        ];
    }
}
