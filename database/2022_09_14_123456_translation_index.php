<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

       
        Schema::table('translation_translations', function (Blueprint $table) {
            $table->unique(['key_id', 'language_id'],'unique_key_lang');
        });
     
       
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('translation_translations', function (Blueprint $table) {
            $table->foreign('key_id')->references('id')->on('translation_keys');

            $table->dropIndex('unique_key_lang');
        });
        Schema::enableForeignKeyConstraints();
    }
};