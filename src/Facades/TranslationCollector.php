<?php 
namespace KDA\Eloquent\I18nCollector\Facades;

use Illuminate\Support\Facades\Facade;


class TranslationCollector extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'translation_collector'; }
}