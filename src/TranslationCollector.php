<?php

namespace KDA\Eloquent\I18nCollector;

use KDA\Eloquent\I18nCollector\Models\Application;
use KDA\Eloquent\I18nCollector\Models\Category;
use KDA\Eloquent\I18nCollector\Models\Key;
use KDA\Eloquent\I18nCollector\Models\KeyVersion;
use KDA\Eloquent\I18nCollector\Models\Language;
use KDA\Eloquent\I18nCollector\Models\Translation;

class TranslationCollector
{

    public function createMissingTranslationForKey(Key $key)
    {
        foreach ($key->category->assignedLanguages->pluck('name', 'id') as $language_id => $lang) {
            $results = Key::whereDoesntHave('translations', function ($q) use ($language_id) {
                $q->where('language_id', $language_id);
            })->get();
            $results->each(function ($key)  use ($language_id) {
                Translation::create([
                    'key_id' => $key->id,
                    'language_id' => $language_id,
                ]);
            });
        }
    }

    public function createMissingTranslationForCategory(Category $category)
    {
        $keys = Key::where('category_id', $category->id)->get();
        $keys->each(function ($key) {
            $this->createMissingTranslationForKey($key);
        });
    }

    public function collect($app, $lang, $category, $key, $version)
    {
        if (!is_array($lang)) {
            $lang = [$lang];
        }
        //$r = Translation::where('app_name',$app)->where('key',$key)->where('namespace',$namespace)->get();

        $app = Application::firstOrCreate(['name' => $app]);
        $category = Category::firstOrCreate(['app_id' => $app->id, 'name' => $category]);
        $key = Key::firstOrCreate(['category_id' => $category->id, 'name' => $key]);
        $version = KeyVersion::firstOrCreate(['key_id'=>$key->id,'version'=>$version]);
        foreach ($lang as $l) {
            $language = Language::firstOrCreate(['key' => $l]);
            $key->touch();
           /* $tr = Translation::where('key_id', $key->id)
                ->where('language_id', $language->id)->get();
            if ($tr->count() == 0) {
                Translation::create([
                    'language_id' => $language->id,
                    'key_id' => $key->id
                ]);
                echo "key created";
            } else {
                echo "key exists";
                $key->touch();
            }*/
        }
    }

    public function retrieve($app, $category, $lang)
    {
        return Translation::with('key')->whereHas('language', function ($q) use ($lang) {
            $q->where('key', $lang);
        })
            ->whereHas('key', function ($q) use ($app, $category) {
                $q->whereHas('category', function ($q) use ($app, $category) {
                    $q->whereHas('app', function ($q) use ($app) {
                        $q->where('name', $app);
                    })->where('name', $category);
                });
            })
            ->get();
    }
}
