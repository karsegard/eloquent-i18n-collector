<?php
namespace KDA\Eloquent\I18nCollector\Models\Relations;

use Illuminate\Database\Eloquent\Relations\Pivot;
use KDA\Eloquent\I18nCollector\Models\Category;
use KDA\Eloquent\I18nCollector\Models\Language;

class AssignedLanguagePivot extends Pivot
{
    protected $table = 'translation_category_languages';
 
    /* -------------------------------------------------------------------------- */
    /*                                  RELATIONS                                 */
    /* -------------------------------------------------------------------------- */

    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }
    public function language()
    {
        return $this->belongsTo(Language::class,'language');
    }
   

}