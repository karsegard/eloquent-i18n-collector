<?php

namespace KDA\Eloquent\I18nCollector\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Application extends Model
{
    use HasFactory;
    protected $table="translation_apps";
    protected $fillable = [
        'name'
    ];

    protected $appends = [
        
    ];

    protected $casts = [
       
    ];
   
    protected static function newFactory()
    {
        return  \KDA\Eloquent\I18nCollector\Database\Factories\ApplicationFactory::new();
    }

    public function categories(){
        return $this->hasMany(Category::class,'app_id');
    }

  
}
