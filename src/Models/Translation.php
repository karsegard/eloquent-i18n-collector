<?php

namespace KDA\Eloquent\I18nCollector\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Translation extends Model
{
    use HasFactory;
    protected $table = "translation_translations";
    protected $fillable = [
        'key_id',
        'language_id',
        'value'
    ];

    protected $appends = [
        'lang',
        'key_name'
    ];

    protected $casts = [
       
    ];

   
    protected static function newFactory()
    {
        return  \KDA\Eloquent\I18nCollector\Database\Factories\TranslationFactory::new();
    }

    public function key(){
        return $this->belongsTo(Key::class,'key_id');
    }


    public function language(){
        return $this->belongsTo(Language::class,'language_id');
    }


    public function getLangAttribute(){
        return $this->language->name;
    }

    public function getKeyNameAttribute(){
        return $this->key->name_without_comment;
    }


    public function scopeNeedingTranslation($q){
        return $q->whereNull('value');
    }

    public function scopeForCollaborator($q,$collaborator){
        $langs = $collaborator->contributableLanguages?->pluck('id') ?? [];
        $cats = $collaborator->contributableCategories?->pluck('id') ?? [];
        return $q->whereHas('language',function($q) use($langs){
            $q->whereIn('id',$langs);
        })->whereHas('key',function ($q) use ($cats){
            $q->whereHas('category',function($q) use($cats){
                $q->whereIn('id',$cats);
            });
        });
    }

    public function scopeWithNoPendingRequests($q){
     
        return $q->whereHas('key',function($q){
             $q->whereDoesntHave('requests',function($q){
                 $q->where('fullfilled',false)->where('requester_id',auth()->user()->id);
            });
        });
    }

}
