<?php

namespace KDA\Eloquent\I18nCollector\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Eloquent\I18nCollector\Models\Relations\AssignedLanguagePivot;

class Category extends Model
{
    use HasFactory;

    protected $table ="translation_categories";


    protected $fillable = [
        'name',
        'app_id'
    ];

    protected $appends = [
        'full_name'
    ];

    protected $casts = [
       'id'=>'integer'
    ];

   
    protected static function newFactory()
    {
        return  \KDA\Eloquent\I18nCollector\Database\Factories\CategoryFactory::new();
    }

    public function app(){
        return $this->belongsTo(Application::class,'app_id');
    }

    public function keys(){
        return $this->hasMany(Key::class,'key_id');
    }
    public function assignedLanguages(){
        return $this->belongsToMany(Language::class,'translation_category_languages')->using(AssignedLanguagePivot::class);
    }

    public function contributors(){
        return $this->morphedByMany(\App\Models\User::class,'contributor','translation_contributor_categories');
    }

    public function getFullNameAttribute(){
        return $this->app->name.'('.$this->name.')';
    }
}
