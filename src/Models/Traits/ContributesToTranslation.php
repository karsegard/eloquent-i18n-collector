<?php
namespace KDA\Eloquent\I18nCollector\Models\Traits;
use KDA\Eloquent\I18nCollector\Models\Language;
use KDA\Eloquent\I18nCollector\Models\Category;

trait ContributesToTranslation
{
    public function contributableLanguages()
    {
        return $this->morphToMany(Language::class, 'contributor', 'translation_contributor_languages');
    }

    public function contributableCategories()
    {
        return $this->morphToMany(Category::class, 'contributor', 'translation_contributor_categories');
    }

   
}
