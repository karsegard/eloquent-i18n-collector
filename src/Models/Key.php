<?php

namespace KDA\Eloquent\I18nCollector\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Key extends Model
{
    use HasFactory;
    protected $table = "translation_keys";

    protected $fillable = [
        'name',
        'comment',
        'category_id'
    ];

    protected $appends = [
        'comment',
        'name_without_comment'
    ];

    protected $casts = [];
    protected static function newFactory()
    {
        return  \KDA\Eloquent\I18nCollector\Database\Factories\KeyFactory::new();
    }

    public function translations()
    {
        return $this->hasMany(Translation::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getNameWithoutCommentAttribute()
    {
        $_state = explode("//", $this->name);
        return $_state[0];
    }

    public function getCommentAttribute()
    {
        if (empty($this->attributes['comment'])) {
            $_state = explode("//", $this->name);
            return $_state[count($_state) - 1];
        }
        return $this->attributes['comment'];
    }

    public function versions()
    {
        return $this->hasMany(KeyVersion::class, 'key_id');
    }

    public function requests()
    {
        return $this->hasMany(Request::class, 'key_id');
    }
}
