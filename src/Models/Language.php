<?php

namespace KDA\Eloquent\I18nCollector\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Eloquent\I18nCollector\Models\Relations\AssignedLanguagePivot;


class Language extends Model
{
    use HasFactory;

    protected $table ="translation_languages";

    protected $fillable = [
        'name',
        'key'
    ];

    protected $appends = [
        
    ];

    protected $casts = [
       
    ];

   
    protected static function newFactory()
    {
        return  \KDA\Eloquent\I18nCollector\Database\Factories\LanguageFactory::new();
    }

    
    public function translations(){
        return $this->hasMany(Translation::class);
    }

    public function assignedCategories(){
        return $this->belongsToMany(Category::class,'translation_category_languages')->using(AssignedLanguagePivot::class);
    }

}
