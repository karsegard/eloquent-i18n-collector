<?php

namespace KDA\Eloquent\I18nCollector\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class KeyVersion extends Model
{
    use HasFactory;
    protected $table="translation_key_versions";
    public $timestamps = false;

    protected $fillable = [
        'key_id',
        'version'
    ];

    

 
}
