<?php

namespace KDA\Eloquent\I18nCollector\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Request extends Model
{
    use HasFactory;
    protected $table="translation_requests";
    protected $fillable = [
        'language_id',
        'key_id',
        'requester_id',
        'author_id',
        'screenshot',
        'fullfilled'
    ];

    protected $appends = [
        
    ];

    protected $casts = [
        'screenshot'=>true,
        'fullfilled'
    ];
   
    protected static function newFactory()
    {
        return  \KDA\Eloquent\I18nCollector\Database\Factories\ApplicationFactory::new();
    }

    public function key(){
        return $this->belongsTo(Key::class,'key_id');
    }
    public function language(){
        return $this->belongsTo(Language::class,'key_id');
    }
    public function requester(){
        return $this->belongsTo(User::class,'requester_id');
    }

    public function author(){
        return $this->belongsTo(User::class,'author_id');
    }
}
