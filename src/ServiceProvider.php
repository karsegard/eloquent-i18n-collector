<?php
namespace KDA\Eloquent\I18nCollector;
use KDA\Laravel\PackageServiceProvider;
//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasLoadableMigration;
    use \KDA\Laravel\Traits\HasDumps;
    use \KDA\Laravel\Traits\HasObservers;


    protected $observers = [
       [Models\Key::class => Observers\KeyObserver::class],
       [Models\Relations\AssignedLanguagePivot::class => Observers\AssignedLanguageObserver::class]
    ];

    protected $dumps = [
        'translation_keys',
        'translation_languages',
        'translation_categories',
        'translation_translations',
        'translation_apps',
        'translation_category_languages',
        'translation_contributor_categories',
        'translation_contributor_languages'
    ];
    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    //-------------------------------------------
        // trait \KDA\Laravel\Traits\HasLoadableMigration
    //registers loadable and not published migrations
    // protected $migrationDir = 'database/migrations';
    public function register()
    {
        parent::register();
        $this->app->singleton('translation_collector',function($app){
            
            return new TranslationCollector();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
