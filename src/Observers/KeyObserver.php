<?php
 
namespace KDA\Eloquent\I18nCollector\Observers;
 
use KDA\Eloquent\I18nCollector\Models\Key;
use KDA\Eloquent\I18nCollector\Facades\TranslationCollector;
 
class KeyObserver
{
    /**
     * Handle the Key "created" event.
     *
     * @param  \App\Models\Key  $Key
     * @return void
     */
    public function created(Key $key)
    {
        TranslationCollector::createMissingTranslationForKey($key);
    }
 
    /**
     * Handle the Key "updated" event.
     *
     * @param  \App\Models\Key  $Key
     * @return void
     */
    public function updated(Key $key)
    {

    }
 
    /**
     * Handle the Key "deleted" event.
     *
     * @param  \App\Models\Key  $Key
     * @return void
     */
    public function deleted(Key $Key)
    {
        //
    }
 
    /**
     * Handle the Key "restored" event.
     *
     * @param  \App\Models\Key  $Key
     * @return void
     */
    public function restored(Key $Key)
    {
        //
    }
 
    /**
     * Handle the Key "forceDeleted" event.
     *
     * @param  \App\Models\Key  $Key
     * @return void
     */
    public function forceDeleted(Key $Key)
    {
        //
    }
}