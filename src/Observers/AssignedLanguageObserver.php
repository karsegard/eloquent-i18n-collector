<?php
 
namespace KDA\Eloquent\I18nCollector\Observers;
 
use KDA\Eloquent\I18nCollector\Models\Relations\AssignedLanguagePivot;
 
use KDA\Eloquent\I18nCollector\Facades\TranslationCollector;
class AssignedLanguageObserver
{
    /**
     * Handle the AssignedLanguagePivot "created" event.
     *
     * @param  \App\Models\AssignedLanguagePivot  $AssignedLanguagePivot
     * @return void
     */
    public function created(AssignedLanguagePivot $pivot)
    {
        TranslationCollector::createMissingTranslationForCategory($pivot->category);
    }
 
    /**
     * Handle the AssignedLanguagePivot "updated" event.
     *
     * @param  \App\Models\AssignedLanguagePivot  $AssignedLanguagePivot
     * @return void
     */
    public function updated(AssignedLanguagePivot $pivot)
    {

    }
 
    /**
     * Handle the AssignedLanguagePivot "deleted" event.
     *
     * @param  \App\Models\AssignedLanguagePivot  $AssignedLanguagePivot
     * @return void
     */
    public function deleted(AssignedLanguagePivot $AssignedLanguagePivot)
    {
        //
    }
 
    /**
     * Handle the AssignedLanguagePivot "restored" event.
     *
     * @param  \App\Models\AssignedLanguagePivot  $AssignedLanguagePivot
     * @return void
     */
    public function restored(AssignedLanguagePivot $AssignedLanguagePivot)
    {
        //
    }
 
    /**
     * Handle the AssignedLanguagePivot "forceDeleted" event.
     *
     * @param  \App\Models\AssignedLanguagePivot  $AssignedLanguagePivot
     * @return void
     */
    public function forceDeleted(AssignedLanguagePivot $AssignedLanguagePivot)
    {
        //
    }
}