<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Eloquent\I18nCollector\Models\Key;
use KDA\Eloquent\I18nCollector\Observers\KeyObserver;
use KDA\Tests\TestCase;

class KeyTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function can_create_model()
  {
      $key =  Key::factory()->create(['name'=>'test']);
      $this->assertNotNull($key);

  }

  /** @test */
  function observer_is_triggered(){
    $key =  Key::factory()->create();
    $o = (object)\Mockery::mock(KeyObserver::class);
   
    $o->shouldReceive('updated')->once();

    \App::instance(KeyObserver::class, $o);

    $key->name="Hello world";
    $key->save();
  }

  
}