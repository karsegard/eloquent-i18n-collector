<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Eloquent\I18nCollector\Models\Language;
use KDA\Eloquent\I18nCollector\Models\Category;
use KDA\Tests\TestCase;

class LanguageTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function can_create_model()
  {
      $lang =  Language::factory()->create(['name'=>'test']);
      $this->assertNotNull($lang);

  }

  /** @test */
  function can_create_models()
  {
    $r = Language::factory()->count(5)->create();
    
    $this->assertDatabaseCount('translation_languages', 5);
  }



  
}