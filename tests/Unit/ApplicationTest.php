<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Eloquent\I18nCollector\Models\Application;
use KDA\Eloquent\I18nCollector\Models\Language;

use KDA\Tests\TestCase;

class ApplicationTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function can_create_model()
  {
      $o =  Application::factory()->create(['name'=>'test']);

      $this->assertNotNull($o);

  }

  
}