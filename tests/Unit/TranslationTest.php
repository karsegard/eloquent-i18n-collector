<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;

use KDA\Eloquent\I18nCollector\Models\Translation;
use KDA\Tests\TestCase;

class TranslationTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function can_create_model()
  {
     
      $translation = Translation::factory()->create();
      $this->assertNotNull($translation);

  }


  /** @test */
  function can_create_models()
  {
     
       Translation::factory()->count(10)->create();
      
      
      $this->assertDatabaseCount('translation_translations', 10);

  }



  
}