<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Eloquent\I18nCollector\Observers\AssignedLanguageObserver;
use KDA\Eloquent\I18nCollector\Models\Key;
use KDA\Eloquent\I18nCollector\Models\Category;
use KDA\Eloquent\I18nCollector\Models\Language;
use KDA\Tests\TestCase;

class PivotTest extends TestCase
{
  use RefreshDatabase;

  
  /** @test */
  function observer_is_triggered(){
   
    $o =  (object)\Mockery::mock(AssignedLanguageObserver::class);
   
    $o->shouldReceive('created')->once();
    \App::instance(AssignedLanguageObserver::class, $o);
    $o =  Category::factory()->create();
    $l =  Language::factory()->create(['name'=>'test']);
    $o->assignedLanguages()->attach($l);
  }


  /** @test */
  function category_can_have_language()
  {
      $o =  Category::factory()->create();
      $l =  Language::factory()->create(['name'=>'test']);
      $o->assignedLanguages()->attach($l);
      $this->assertNotNull($o);
      $this->assertEquals($o->assignedLanguages->first()->id,$l->id);

  }

  /** @test */
  function lang_can_have_categories()
  {
      $o =  Category::factory()->create();
      $l =  Language::factory()->create(['name'=>'test']);
      $l->assignedCategories()->attach($o);
      $this->assertEquals($l->assignedCategories->first()->id,$o->id);

      

      
  }


}