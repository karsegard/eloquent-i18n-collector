<?php

namespace KDA\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Eloquent\I18nCollector\Observers\AssignedLanguageObserver;
use KDA\Eloquent\I18nCollector\Models\Key;
use KDA\Eloquent\I18nCollector\Models\Category;
use KDA\Eloquent\I18nCollector\Models\Language;
use KDA\Eloquent\I18nCollector\Models\Translation;
use KDA\Tests\TestCase;

class PivotTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function auto_create_translation()
  {
      $o =  Category::factory()->create();
      $l =  Language::factory()->create(['name'=>'test']);
      $l->assignedCategories()->attach($o);
      

      $k = Key::factory()->create(['category_id'=>$o->id]);

      $tr = Translation::where('key_id',$k->id)->get();
      $this->assertEquals($tr->count(),1);

  }


  /** @test */
  function auto_create_translation_mixed_created()
  {
      $o =  Category::factory()->create();
      $fr =  Language::factory()->create(['name'=>'fr']);
      $en =  Language::factory()->create(['name'=>'fr']);
      $fr->assignedCategories()->attach($o);
      $o->assignedLanguages()->attach($en);
      

      $k = Key::factory()->create(['category_id'=>$o->id]);

      $tr = Translation::where('key_id',$k->id)->get();
      $this->assertEquals($tr->count(),2);

  }


  /** @test */
  function auto_create_translation_mixed_pivot()
  {
      

      $o =  Category::factory()->create();
      $k = Key::factory()->create(['category_id'=>$o->id]);
      $fr =  Language::factory()->create(['name'=>'fr']);
      $en =  Language::factory()->create(['name'=>'fr']);
      $fr->assignedCategories()->attach($o);
      $o->assignedLanguages()->attach($en);
      


      $tr = Translation::where('key_id',$k->id)->get();
      $this->assertEquals($tr->count(),2);

  }
}